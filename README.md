# Jakob

## Why call this Jakob?

Everything needs a name, so Jakob is as good as anything.

## What is this for?

This is for me to play around and learn Rust at the same time as learning how neural networks work.

## What can I do with this?

Whatever you want. Use it, fork it, change it, whatever...
