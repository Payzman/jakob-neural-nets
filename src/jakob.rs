pub mod activation_functions;
pub mod neuron;
pub mod neural_net;
pub mod target;
pub mod connection;
pub mod math;