use std::f32::consts::E;

pub fn identity_function(val: f32) -> f32 {
    return val;
}

pub fn binary_step_function(val: f32) -> f32 {
    if val < 0.0 {
        return 0.0
    }
    return 1.0
}

pub fn binary_sigmoid_function(val: f32) -> f32 {
    return 1.0 / (1.0 + E.powf(-val));
}

pub fn bipolar_sigmoid_function(val: f32) -> f32 {
    return 2.0 * binary_sigmoid_function(val) - 1.0;
}