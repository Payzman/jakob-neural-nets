use super::neuron::Neuron;

pub struct Connection<'a> {
    pub start_neuron: &'a Neuron,
    pub end_neuron: &'a Neuron,
    pub weight: f32
}