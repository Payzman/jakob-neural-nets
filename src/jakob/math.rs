pub fn add_vector_f32_vector_f32(a: &Vec<f32>, b: &Vec<f32>) -> Vec<f32> {
    let mut result = vec![0.0; a.len()];
    for i in 0..a.len() {
        result[i] = a[i] + b[i];
    }
    return result;
}

pub fn mul_matrix_f32_vector_bool(matrix: &Vec<Vec<f32>>, vector: &Vec<bool>) -> Vec<f32> {
    let mut result = vec![0.0; vector.len()];
    for i in 0..matrix.len() {
        for j in 0..matrix[i].len() {
            result[i] += matrix[j][i] * bool_to_f32(vector[j]);
        }
    }
    return result;
}

pub fn bool_to_f32(b: bool) -> f32 {
    if b {
        return 1.0;
    }
    return 0.0;
}

pub fn geq_vector_f32_vector_f32(a: &Vec<f32>, b: &Vec<f32>) -> Vec<bool> {
    let mut result = vec![false; a.len()];
    for i in 0..a.len() {
        result[i] = a[i] >= b[i];
    }
    return result;
}