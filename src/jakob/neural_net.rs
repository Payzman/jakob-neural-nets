use super::{neuron::Neuron, connection::Connection, math::*};

pub struct NeuralNet {
    pub identifiers: Vec<String>,
    pub activation_vector: Vec<bool>,
    pub input_signal_vector: Vec<f32>,
    pub threshold_vector: Vec<f32>,
    pub weight_matrix: Vec<Vec<f32>>,
    pub input_bias: Vec<f32>
}

pub fn create_neural_net(neurons: Vec<&Neuron>, connections: Vec<Connection>) -> NeuralNet {
    let amount_neurons = neurons.len();
    let mut identifiers = vec![String::from(""); amount_neurons];
    let activation_vector = vec![false; amount_neurons];
    let input_signal_vector = vec![0.0; amount_neurons];
    let mut threshold_vector = vec![0.0; amount_neurons];
    let mut weight_matrix = vec![vec![0.0; amount_neurons]; amount_neurons];
    let input_bias = vec![0.0; amount_neurons];

    for i in 0..amount_neurons {
        identifiers[i] = neurons[i].identifier.clone();
        threshold_vector[i] = neurons[i].threshold;
    }

    for i in 0..connections.len() {
        let start_idx = neurons.iter().position(|n| n.identifier == connections[i].start_neuron.identifier);
        let end_idx = neurons.iter().position(|n| n.identifier == connections[i].end_neuron.identifier);
        weight_matrix[start_idx.unwrap()][end_idx.unwrap()] = connections[i].weight;
    }

    return NeuralNet { identifiers, activation_vector, input_signal_vector, threshold_vector, weight_matrix, input_bias }
}

pub fn step_neural_net(net: &mut NeuralNet) {
    net.input_signal_vector = mul_matrix_f32_vector_bool(&net.weight_matrix, &net.activation_vector);
    net.input_signal_vector = add_vector_f32_vector_f32(&net.input_signal_vector, &net.input_bias);
    net.activation_vector = geq_vector_f32_vector_f32(&net.input_signal_vector, &net.threshold_vector);
}