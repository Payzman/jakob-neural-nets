pub struct Neuron {
    pub identifier: String,
    pub threshold: f32
}