#[derive(Debug, Clone, Copy)]
pub struct Target {
    value: usize
}

impl Target {
    pub fn new(val: usize) -> Self {
        Self { value: val }
    }
}

impl From<Target> for usize {
    fn from(target: Target) -> Self {
        return target.value;
    }
}