#[cfg(test)]
mod tests {
    use jakob_neural_nets::jakob::activation_functions::{identity_function, binary_step_function, binary_sigmoid_function, bipolar_sigmoid_function};

    #[test]
    fn test_identity_function() {
        // arrange
        let inputs = [-1.99, -2.34, 6.17, -4.91, 2.49];
        let expected = [
            -1.99,
            -2.34,
            6.17,
            -4.91,
            2.49];
        let mut outputs = [0.0; 5];

        // act
        for i in 0..5 {
            outputs[i] = identity_function(inputs[i])
        }

        // assert
        for i in 0..5 {
            assert_eq!(expected[i], outputs[i])
        }
    }

    #[test]
    fn test_binary_step_function() {
        // arrange
        let inputs = [-1.99, -2.34, 6.17, -4.91, 2.49];
        let expected = [
            0.0,
            0.0,
            1.0,
            0.0,
            1.0];
        let mut outputs = [0.0; 5];

        // act
        for i in 0..5 {
            outputs[i] = binary_step_function(inputs[i])
        }

        // assert
        for i in 0..5 {
            assert_eq!(expected[i], outputs[i])
        }
    }

    #[test]
    fn test_binary_sigmoid_function() {
        // arrange
        let inputs = [-1.99, -2.34, 6.17, -4.91, 2.49];
        let expected = [
            0.120,
            0.088,
            0.998,
            0.007,
            0.923];
        let mut outputs = [0.0; 5];

        // act
        for i in 0..5 {
            outputs[i] = binary_sigmoid_function(inputs[i])
        }

        // assert
        for i in 0..5 {
            let diff: f32 = (expected[i] - outputs[i]).into();
            assert!(diff.abs() < 1e-3);
        }
    }

    #[test]
    fn test_bipolar_sigmoid_function() {
        // arrange
        let inputs = [-1.99, -2.34, 6.17, -4.91, 2.49];
        let expected = [
            -0.759,
            -0.824,
            0.996,
            -0.985,
            0.847];
        let mut outputs = [0.0; 5];

        // act
        for i in 0..5 {
            outputs[i] = bipolar_sigmoid_function(inputs[i])
        }

        // assert
        for i in 0..5 {
            let diff: f32 = (expected[i] - outputs[i]).into();
            assert!(diff.abs() < 1e-3);
        }
    }
}