#[cfg(test)]
mod tests {
    use jakob_neural_nets::jakob::{neuron::Neuron, connection::Connection, neural_net::{create_neural_net, step_neural_net, NeuralNet}};

    #[test]
    fn simple_mcculloch_pitts_neuron() {
        let x1 = Neuron { identifier:String::from("x1"), threshold:0.0 };
        let x2 = Neuron { identifier:String::from("x2"), threshold:0.0 };
        let x3 = Neuron { identifier:String::from("x3"), threshold:0.0 };

        let y = Neuron { identifier:String::from("y"), threshold:4.0 };

        let neurons = vec![&x1, &x2, &x3, &y];
        let connections = vec![
            Connection { start_neuron:&x1, end_neuron:&y, weight:2.0 },
            Connection { start_neuron:&x2, end_neuron:&y, weight:2.0 },
            Connection { start_neuron:&x3, end_neuron:&y, weight:-1.0 }
        ];

        let net = &mut create_neural_net(neurons, connections);
        step_neural_net(net);
        step_neural_net(net);
        for i in 0..net.identifiers.len() {
            println!("Neuron {} activation={}", net.identifiers[i], net.activation_vector[i]);
        }
    }

    #[test]
    fn logical_and() {
        let test_params = [(false, false), (false, true), (true, false), (true, true)];
        let test_results = [false, false, false, true];

        for i in 0..test_params.len() {
            let mut x1 = Neuron { identifier:String::from("x1"), threshold:1.0 };
            if test_params[i].0 {
                x1.threshold = 0.0;
            }
            let mut x2 = Neuron { identifier:String::from("x2"), threshold:1.0 };
            if test_params[i].1 {
                x2.threshold = 0.0;
            }

            let y = Neuron { identifier:String::from("y"), threshold:2.0 };

            let neurons = vec![&x1, &x2, &y];
            let connections = vec![
                Connection { start_neuron:&x1, end_neuron:&y, weight:1.0 },
                Connection { start_neuron:&x2, end_neuron:&y, weight:1.0 }
            ];

            let net = &mut create_neural_net(neurons, connections);
            
            step_neural_net(net); // init
            step_neural_net(net);
            assert_eq!(net.activation_vector[2], test_results[i]);
        }
    }
    
    #[test]
    fn logical_or() {
        let test_params = [(false, false), (false, true), (true, false), (true, true)];
        let test_results = [false, true, true, true];

        for i in 0..test_params.len() {
            let mut x1 = Neuron { identifier:String::from("x1"), threshold:1.0 };
            if test_params[i].0 {
                x1.threshold = 0.0;
            }
            let mut x2 = Neuron { identifier:String::from("x2"), threshold:1.0 };
            if test_params[i].1 {
                x2.threshold = 0.0;
            }

            let y = Neuron { identifier:String::from("y"), threshold:2.0 };

            let neurons = vec![&x1, &x2, &y];
            let connections = vec![
                Connection { start_neuron:&x1, end_neuron:&y, weight:2.0 },
                Connection { start_neuron:&x2, end_neuron:&y, weight:2.0 }
            ];

            let net = &mut create_neural_net(neurons, connections);
            
            step_neural_net(net); // init
            step_neural_net(net);
            assert_eq!(net.activation_vector[2], test_results[i]);
        }
    }
    
    #[test]
    fn logical_and_not() {
        let test_params = [(false, false), (false, true), (true, false), (true, true)];
        let test_results = [false, false, true, false];

        for i in 0..test_params.len() {
            let mut x1 = Neuron { identifier:String::from("x1"), threshold:1.0 };
            if test_params[i].0 {
                x1.threshold = 0.0;
            }
            let mut x2 = Neuron { identifier:String::from("x2"), threshold:1.0 };
            if test_params[i].1 {
                x2.threshold = 0.0;
            }

            let y = Neuron { identifier:String::from("y"), threshold:2.0 };

            let neurons = vec![&x1, &x2, &y];
            let connections = vec![
                Connection { start_neuron:&x1, end_neuron:&y, weight:2.0 },
                Connection { start_neuron:&x2, end_neuron:&y, weight:-1.0 }
            ];

            let net = &mut create_neural_net(neurons, connections);
            
            step_neural_net(net); // init
            step_neural_net(net);
            assert_eq!(net.activation_vector[2], test_results[i]);
        }
    }

    #[test]
    fn logical_xor() {
        let test_params = [(false, false), (false, true), (true, false), (true, true)];
        let test_results = [false, true, true, false];

        for i in 0..test_params.len() {
            let mut x1 = Neuron { identifier:String::from("x1"), threshold:1.0 };
            if test_params[i].0 {
                x1.threshold = 0.0;
            }
            let mut x2 = Neuron { identifier:String::from("x2"), threshold:1.0 };
            if test_params[i].1 {
                x2.threshold = 0.0;
            }

            let z1 = Neuron { identifier:String::from("z1"), threshold:2.0 };
            let z2 = Neuron { identifier:String::from("z2"), threshold:2.0 };

            let y = Neuron { identifier:String::from("y"), threshold:2.0 };

            let neurons = vec![&x1, &x2, &z1, &z2, &y];
            let connections = vec![
                Connection { start_neuron:&x1, end_neuron:&z1, weight:2.0 },
                Connection { start_neuron:&x1, end_neuron:&z2, weight:-1.0 },
                Connection { start_neuron:&x2, end_neuron:&z1, weight:-1.0 },
                Connection { start_neuron:&x2, end_neuron:&z2, weight:2.0 },
                Connection { start_neuron:&z1, end_neuron:&y, weight:2.0 },
                Connection { start_neuron:&z2, end_neuron:&y, weight:2.0 },
            ];

            let net = &mut create_neural_net(neurons, connections);
            
            step_neural_net(net); // init (input layer)
            step_neural_net(net); // inner layer activated
            step_neural_net(net); // output layer
            assert_eq!(net.activation_vector[4], test_results[i]);
        }
    }

    fn create_net_for_perception_of_hot_and_hold() -> NeuralNet {
        let x1 = Neuron { identifier:String::from("x1"), threshold:1.0 };
        let x2 = Neuron { identifier:String::from("x2"), threshold:1.0 };
        let z1 = Neuron { identifier:String::from("z1"), threshold:2.0 };
        let z2 = Neuron { identifier:String::from("z2"), threshold:2.0 };
        let y1 = Neuron { identifier:String::from("y1"), threshold:2.0 };
        let y2 = Neuron { identifier:String::from("y2"), threshold:2.0 };
        let neurons = vec![
            &x1,
            &x2,
            &z1,
            &z2,
            &y1,
            &y2,
        ];

        let connections = vec![
            Connection { start_neuron:&x1, end_neuron:&y1, weight:2.0 },
            Connection { start_neuron:&x2, end_neuron:&z1, weight:-1.0 },
            Connection { start_neuron:&x2, end_neuron:&z2, weight:2.0 },
            Connection { start_neuron:&x2, end_neuron:&y2, weight:1.0 },
            Connection { start_neuron:&z1, end_neuron:&y1, weight:2.0 },
            Connection { start_neuron:&z2, end_neuron:&z1, weight:2.0 },
            Connection { start_neuron:&z2, end_neuron:&y2, weight:1.0 },
        ];

        return create_neural_net(neurons, connections);
    }

    #[test]
    fn perception_test_hot() {
        let net = &mut create_net_for_perception_of_hot_and_hold();
        net.input_bias = vec![1.0, 0.0, 0.0, 0.0, 0.0, 0.0];
        step_neural_net(net); // t = 0
        step_neural_net(net); // t = 1
        assert!(net.activation_vector[4]); // hot perception
    }

    #[test]
    fn perception_test_cold_short() {
        let net = &mut create_net_for_perception_of_hot_and_hold();
        net.input_bias = vec![0.0, 1.0, 0.0, 0.0, 0.0, 0.0];
        step_neural_net(net); // t = 0
        net.input_bias = vec![0.0; 6];
        step_neural_net(net); // t = 1
        step_neural_net(net); // t = 2
        step_neural_net(net); // t = 3
        assert!(net.activation_vector[4]); // hot perception
    }

    #[test]
    fn perception_test_cold_long() {
        let net = &mut create_net_for_perception_of_hot_and_hold();
        net.input_bias = vec![0.0, 1.0, 0.0, 0.0, 0.0, 0.0];
        step_neural_net(net); // t = 0
        step_neural_net(net); // t = 1
        step_neural_net(net); // t = 2
        assert!(net.activation_vector[5]); // cold perception
    }
}